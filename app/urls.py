from django.urls import path
from . import views

urlpatterns = [

	path('', views.inicio, name='inicio'),
	path('login/', views._login, name='login'),
	path('logout/', views._logout, name='logout'),
	path('registro/', views.registro, name='registro'),
	path('password_change/', views.password_change, name='password_change'),
	path('.*/', views.inicio, name='inicio'),


]
