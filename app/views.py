from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import Group, User

# Create your views here.

def _login(request):

	if request.method == 'POST':

		usuario = request.POST['usuario']
		
		password = request.POST['password']

		user = authenticate(username=usuario, password=password)

		if user is not None:

			if user.is_active:

				login(request, user)
	
				return HttpResponseRedirect("/")

		else:

			return render(request, 'login.html', {'info':'Usuario Password Incorrecto'})



	return render(request, 'login.html', {})





@login_required(login_url="/login")
def _logout(request):

	logout(request)
	
	return HttpResponseRedirect("/")




@login_required(login_url="/login")
def inicio(request):

	usuario=request.user.username

	return render(request, 'inicio.html', {'usuario':usuario})



def password_change(request):
    return render(request, 'password_change.html', {})



def registro(request):

	if request.method == 'POST':


		try:

			usuario = request.POST['usuario']
			
			password = request.POST['password']

			user = User.objects.create_user(username=usuario,password=password)

			user.save()

			user = authenticate(username=usuario, password=password)

			login(request, user)

			return HttpResponseRedirect("/")

		except:

			return render(request, 'registro.html', {'info':'Ese usuario ya existe '})





	return render(request, 'registro.html', {})




